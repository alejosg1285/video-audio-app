package com.example.audiovideo;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    //Audio
    Button btnBuscarVideo;
    Button btnPausarVideo;
    Button btnResumirVideo;
    Button btnPararVideo;
    //Video
    Button btnBuscarAudio;
    Button btnPausarAudio;
    Button btnResumirAudio;
    Button btnPararAudio;
    //Grabación de audio
    Button btnGrabarAudio;
    Button btnPararGrabacion;
    //videoView objeto de tipo VideoView, para reproducir videos
    VideoView videoView;
    //mediaController, objeto de tipo mediaController, sirve para reproducir videos
    MediaController mediaController;
    //mp, objeto de tipo MediaPlayer, sirve para reproducir audios
    MediaPlayer mp = new MediaPlayer();
    //recorder, objeto de tipo MediaRecorder, para grabar audio
    MediaRecorder recorder;
    //Referencia del archivo a ser grabado (nombre)
    File audiofile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Referenciación de los botónes en el layout
        btnBuscarVideo = (Button) findViewById(R.id.btnBuscarVideo);
        btnResumirVideo = (Button) findViewById(R.id.btnResumirVideo);
        btnPararVideo = (Button) findViewById(R.id.btnPararVideo);
        btnPausarVideo = (Button) findViewById(R.id.btnPausarVideo);
        btnBuscarAudio = (Button) findViewById(R.id.btnBuscarAudio);
        btnResumirAudio = (Button) findViewById(R.id.btnResumirAudio);
        btnPararAudio = (Button) findViewById(R.id.btnPararAudio);
        btnPausarAudio = (Button) findViewById(R.id.btnPausarAudio);
        btnGrabarAudio = (Button) findViewById(R.id.btnGrabarAudio);
        btnPararGrabacion = (Button) findViewById(R.id.btnPararGrabacion);
//Referenciación del objeto videoView en el layout
        videoView = (VideoView) findViewById(R.id.videoView1);
//Instancia de la clase MediaController, para reproducir videos
        mediaController = new MediaController(getBaseContext());
//Fijación del ancho del video
        mediaController.setAnchorView(videoView);

//Verificación de permisos para escribir al almacenamiento externo del celular (SD memory)
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
//Solicitud del permiso
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            return;
        }

        //Verificación de permisos para acceder al almacenamiento externo del celular (SD memory)
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
//Solicitud del permiso
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            return;
        }
//Verificación de permisos para grabar audio
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
//Solicitud del permiso
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO}, 1);
            return;
        }

        //Nota: recuerde que la función CallBack para saber si un permiso fue o no concedido es onRequestPermissionsResult (esta mas abajo)

//Botón btnGrabarAudio, grabación de audio
        btnGrabarAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//recorder, objeto de tipo MediaRecorder para grabar Audio
                recorder = new MediaRecorder();
                btnGrabarAudio.setEnabled(false); //Deshabilitar el botón
                btnPararGrabacion.setEnabled(true); //Habilitar el botón
//Directorio de almacenamiento
                File dir = Environment.getExternalStorageDirectory();
                try {
//Nombre del archivo
                    audiofile = File.createTempFile("sonido", ".3gp", dir);

                    recorder.setAudioSource(MediaRecorder.AudioSource.MIC); //Fuente de grabación (MIC = Microfono)
                    recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP); //Formato de salida en este caso ".3gp"
                    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB); //Audio Encoder
                    recorder.setOutputFile(audiofile.getAbsolutePath()); //Ruta de almacenamiento
                    recorder.prepare(); //preperación
                    recorder.start(); //Inicio de la grabación
                } catch (IOException e) {
                    Log.d("Error", "Error al intentar acceder a la memoria externa: " + e.getMessage());
                    return;
                }
            }
        });

        //Botón parar grabación de audio
        btnPararGrabacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnGrabarAudio.setEnabled(true); //Habilitar el botón
                btnPararGrabacion.setEnabled(false); //Deshabilitar el botón
//Detener audio
                recorder.stop();
//Liberar el recorder
                recorder.release();
                ContentValues values = new ContentValues(4);
                long current = System.currentTimeMillis(); //Obtención de los milisegundos actuales
                values.put(MediaStore.Audio.Media.TITLE, "audio" + audiofile.getName()); //Titulo
                values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000)); //Fecha
                values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/3gpp"); //MIME_TYPE
                values.put(MediaStore.Audio.Media.DATA, audiofile.getAbsolutePath()); //Directorio
//creating content resolver and storing it in the external content uri
                ContentResolver contentResolver = getContentResolver();
                Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                Uri newUri = contentResolver.insert(base, values);
//sending broadcast message to scan the media file so that it can be available
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
                Toast.makeText(getBaseContext(), "Archivo guardado " + newUri, Toast.LENGTH_LONG).show();
            }
        });

        /* btnGrabarAudio.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
if (videoView.isPlaying()) {
Toast.makeText(getBaseContext(), "Pausado", Toast.LENGTH_LONG);
videoView.pause();
}
}
}); */

//Pausar la reproducción del video
        btnPausarVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//Si el video se esta reproducciendo
                if (videoView.isPlaying()) {
                    Toast.makeText(getBaseContext(), "Pausado", Toast.LENGTH_LONG);
                    videoView.pause();
                }
            }
        });
//Detener la reproducción del video
        btnPararVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//Detener la reproducción del video
                videoView.stopPlayback();
            }
        });
//Resumir la reproducción del video
        btnResumirVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//Verificación de que el video no este en reproducción
                if (!videoView.isPlaying()) {
                    videoView.start();
                }
            }
        });

        //Botón para buscar videos
        btnBuscarVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT); //Intent para obtener contenidos del celular
                intent.addCategory(Intent.CATEGORY_OPENABLE); //Categoria de archivos
// intent.setType("*/*");
//intent.setType("image/*");
// intent.setType("audio/*");
                intent.setType("video/*"); //Filtro
// String[] mimetypes = {"text/comma-separated-values", "text/csv"};
// intent.putExtra(EXTRA_MIME_TYPES, mimetypes);
// intent.setType("image/*");
//Creating MediaController
                try {
                    startActivityForResult(intent, 1);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getBaseContext(), "El celular no tiene un explorador de archivos instalado.",

                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Botón para buscar audios
        btnBuscarAudio.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
// intent.setType("*/*");
//intent.setType("image/*");
// intent.setType("audio/*");
                intent.setType("audio/*"); //Filtro
// String[] mimetypes = {"text/comma-separated-values", "text/csv"};
// intent.putExtra(EXTRA_MIME_TYPES, mimetypes);
// intent.setType("image/*");
//Creating MediaController
                try {
                    startActivityForResult(intent, 2);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getBaseContext(), "El celular no tiene un explorador de archivos instalado.",

                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Botón para pausar la reproducción del audio
        btnPausarAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//Verificar si hay una reproducción en curso
                if (mp.isPlaying()) {
                    Toast.makeText(getBaseContext(), "Pausado", Toast.LENGTH_LONG);
                    mp.pause();
                }
            }
        });
//Botón para parar la reproducción de audio
        btnPararAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//Verificar si hay una reproducción en curso
                if (mp.isPlaying()) {
                    mp.stop();
                }
            }
        });

        //Botón para resumir reproducción de audio
        btnResumirAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//Verificar que no hay una reproducción en curso
                if (!mp.isPlaying()) {
                    mp.start();
                }
            }
        });

//Ejemplos
//https://www.programcreek.com/java-api-examples/?class=android.content.Intent&method=ACTION_GET_CONTENT
    }

    @Override //CallBack para gestionar las respuestas de solicitud de permisos
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
/* if (requestCode == 1) {
if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//intent.setType("image/*");
//intent.setType("audio/*");
// startActivityForResult(intent,PICK_PHOTO);
} else {
// Permission Denied
// Toast.makeText(ObjectCardActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
}
}*/
// if (requestCode == 1) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Permiso permitido !",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Permiso denegado !",
                    Toast.LENGTH_LONG).show();
        }
// }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //CallBack para gestionar el intent especial de Audio y Video
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
//Si el intent es para Video
            if (requestCode == 1) {
                try {
//URI(Uniform resource identifier)
                    Uri uri = data.getData();
                    String filename = uri.toString();
// Toast.makeText(getBaseContext(), filename, Toast.LENGTH_SHORT).show();
// String FilePath = data.getData().getPath();
// Toast.makeText(getBaseContext(), FilePath, Toast.LENGTH_SHORT).show();
                    //specify the location of media file
                    Uri uri1 = Uri.parse(filename);
//Setting MediaController and URI, then starting the videoView
                    videoView.setMediaController(mediaController);
                    videoView.setVideoURI(uri1);
                    videoView.requestFocus();
                    videoView.start();

// videoView.pause();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Si el intent es para Audio
            if (requestCode == 2) {
                try {
//URI(Uniform resource identifier)
                    Uri uri = data.getData();
                    String filename = uri.toString(); //Nombre del archivo
// Toast.makeText(getBaseContext(), filename, Toast.LENGTH_SHORT).show();
// String FilePath = data.getData().getPath();
// Toast.makeText(getBaseContext(), FilePath, Toast.LENGTH_SHORT).show();

//specify the location of media file
                    Uri uri1 = Uri.parse(filename);
                    try {
//you can change the path, here path is external directory(e.g. sdcard) /Music/maine.mp3
                        mp.setDataSource(getBaseContext(), uri);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
// videoView.pause();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
